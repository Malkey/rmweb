import React from 'react';
import {StyledFloatingSection, StyledJumbotron} from './FloatingSection.styles';

const FloatingSection: React.FC = ({children}) => {
  return (
    <StyledFloatingSection>
      <StyledJumbotron>{children}</StyledJumbotron>
    </StyledFloatingSection>
  );
};

export default FloatingSection;
